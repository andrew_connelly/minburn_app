<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{

    public function testADefaultUserIsNotAnAdmin()
    {
        $user = factory(User::class)->create();

        $this->assertFalse($user->isAdmin());
    }

    public function testAnAdminUserIsAnAdmin()
    {
        $admin = factory(User::class)
            ->states('admin')
            ->create();
        $this->assertTrue($admin->isAdmin());
    }
}
